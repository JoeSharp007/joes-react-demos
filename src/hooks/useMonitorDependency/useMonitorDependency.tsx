import { useEffect } from 'react';
import useCounter from '../useCounter';

const useMonitorDependency = (dependency: unknown) => {
    const {count, increment} = useCounter();

    useEffect(increment, [dependency]);

    return count;
}

export default useMonitorDependency;

