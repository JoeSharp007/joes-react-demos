import { useCallback, useState } from "react";
import { UseCounter } from "./useCounter";

const useCounterWithState = (initialValue: number = 0): UseCounter => {
  const [count, setCount] = useState<number>(initialValue);

  const increment = useCallback(() => setCount((c) => c + 1), [setCount]);
  const decrement = useCallback(() => setCount((c) => c - 1), [setCount]);

  return {
    count,
    increment,
    decrement,
  };
};

export default useCounterWithState;
