import { FC } from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import useCounter from "../../hooks/useCounter/useCounter";

interface Props {
  initialValue?: number;
}

const TestHarness: FC<Props> = ({ initialValue = 0 }) => {
  const { count, increment, decrement } = useCounter(initialValue);

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

export default {
  title: "hooks/useCounter",
  component: TestHarness,
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof TestHarness>;

const Template: ComponentStory<typeof TestHarness> = (args) => (
  <TestHarness {...args} />
);

export const Primary = Template.bind({});

export const LargeInitialValue = Template.bind({});
LargeInitialValue.args = {
  initialValue: 100,
};
