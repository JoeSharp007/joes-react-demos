import { renderHook, act } from "@testing-library/react";
import useCounter, { UseCounter } from "./useCounter";
import useCounterWithState from "./useCounterWithState";

type CounterHook = (initialValue?: number) => UseCounter;

const hooks: [string, CounterHook][] = [
  ["useCounter", useCounter],
  ["useCounterWithState", useCounterWithState],
];

describe.each(hooks)("%s", (_, hook) => {
  it("should increment counter", () => {
    const { result } = renderHook(() => hook());

    act(() => {
      result.current.increment();
    });

    expect(result.current.count).toBe(1);
  });
  it("should decrement counter", () => {
    const { result } = renderHook(() => hook());

    act(() => {
      result.current.decrement();
    });

    expect(result.current.count).toBe(-1);
  });
  it("should stack increments and decrements correctly", () => {
    const { result } = renderHook(() => hook());

    act(() => {
      result.current.increment(); // 1
      result.current.increment(); // 2
      result.current.increment(); // 3
      result.current.decrement(); // 2
      result.current.decrement(); // 1
      result.current.increment(); // 2
    });

    expect(result.current.count).toBe(2);
  });
  it("should respect initial value", () => {
    const { result } = renderHook(() => hook(1000));

    act(() => {
      result.current.increment(); // 1
      result.current.increment(); // 2
      result.current.increment(); // 3
    });

    expect(result.current.count).toBe(1003);
  });
});
