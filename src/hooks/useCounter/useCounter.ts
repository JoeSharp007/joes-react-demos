import { useCallback, useReducer } from "react";

export interface UseCounter {
  count: number;
  increment: () => void;
  decrement: () => void;
}

type CounterAction = "increment" | "decrement";

const reducer = (state: number, action: CounterAction): number => {
  switch (action) {
    case "increment":
      return state + 1;
    case "decrement":
      return state - 1;
  }
};

const useCounter = (initialValue: number = 0): UseCounter => {
  const [count, dispatch] = useReducer(reducer, initialValue);

  const increment = useCallback(() => dispatch("increment"), []);
  const decrement = useCallback(() => dispatch("decrement"), []);

  return {
    count,
    increment,
    decrement,
  };
};

export default useCounter;
