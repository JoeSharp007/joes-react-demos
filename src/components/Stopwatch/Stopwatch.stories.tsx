import React, { FC } from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import Stopwatch from "./Stopwatch";

export default {
  title: "components/Stopwatch",
  component: Stopwatch,
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof Stopwatch>;

const Template: ComponentStory<typeof Stopwatch> = () => <Stopwatch />;

export const Primary = Template.bind({});
