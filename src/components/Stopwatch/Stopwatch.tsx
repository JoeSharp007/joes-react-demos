import { FC, useState, useRef, useCallback, useEffect } from "react";

const Stopwatch: FC = () => {
  const timerIdRef = useRef<NodeJS.Timer>();
  const [count, setCount] = useState(0);

  const onStart = useCallback(() => {
    if (timerIdRef.current) return;

    timerIdRef.current = setInterval(() => setCount((c) => c + 1), 1000);
  }, [setCount]);

  const onStop = useCallback(() => {
    clearInterval(timerIdRef.current);
    timerIdRef.current = undefined;
  }, []);

  useEffect(() => {
    return () => clearInterval(timerIdRef.current);
  }, []);

  return (
    <div>
      <div>Timer: {count}</div>
      <div>
        <button onClick={onStart}>Start</button>
        <button onClick={onStop}>Stop</button>
      </div>
      <p>{!!timerIdRef.current ? "Running" : "Stopped"}</p>
    </div>
  );
};

export default Stopwatch;
