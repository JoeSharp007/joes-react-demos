import { FC, MouseEventHandler, useRef, useState, useCallback } from 'react';
import DependencyChangeCounter from './DependencyChangeCounter';

/**
 * Here I have updated the calls to useState with functions.
 * useState can accept a function, the single argument is the current state, which may be required
 * for calculating the new value. This 'pure function' is MUCH better because
 * the deleteMember & addMember functions no longer 'depend' on the current state.
 * 
 * @returns Component
 */
const TeamBuilder: FC = () => {
    const [team, setTeam] = useState<string[]>(['alpha', 'bravo', 'charlie']);
    const txtNewMemberName = useRef<HTMLInputElement>(null);

    const deleteMember = useCallback((member: string) => {
        setTeam(t => t.filter(m => m !== member));
    }, [setTeam])

    const addMember: MouseEventHandler = useCallback((e) => {
        e.preventDefault();
        if (!!txtNewMemberName.current) {
            setTeam(t => [...t, txtNewMemberName.current!.value])
        }
    }, [setTeam]);

    return <div>
        <h1>Team Builder With Memoisation</h1>
        <DependencyChangeCounter {...{addMember, deleteMember}} />

        <h2>Members</h2>
        <ul>
            {team.map(member => (
                <li>
                    {member}
                    <button onClick={() => deleteMember(member)}>Delete</button>
                </li>
            ))}
        </ul>

        <form>
                <div>
                    <label htmlFor='new-member-name'>New Member Name</label>
                    <input ref={txtNewMemberName} name='new-member-name'></input>
                </div>
                <button onClick={addMember}>Add</button>
        </form>
    </div>
}

export default TeamBuilder;