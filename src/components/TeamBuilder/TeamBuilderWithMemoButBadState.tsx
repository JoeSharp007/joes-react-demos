import { FC, MouseEventHandler, useRef, useState, useCallback } from 'react';
import DependencyChangeCounter from './DependencyChangeCounter';

/**
 * As a first improvement, I have wrapped all the handlers in useCallback.
 * 
 * This tells React only to regenerate the function if something in our dependency array changes.
 * Functions like state setters and reducer dispatch, are not generally regenerated, so once these functions
 * have been built, they don't trigger re-renders.
 * 
 * I have left the state setting using the existing value of 'team' which is not good.
 * 
 * @returns Component
 */
const TeamBuilder: FC = () => {
    const [team, setTeam] = useState<string[]>(['alpha', 'bravo', 'charlie']);
    const txtNewMemberName = useRef<HTMLInputElement>(null);

    const deleteMember = useCallback((member: string) => {
        setTeam(team.filter(m => m !== member));
    }, [team, setTeam])

    const addMember: MouseEventHandler = useCallback((e) => {
        e.preventDefault();
        if (!!txtNewMemberName.current) {
            setTeam([...team, txtNewMemberName.current!.value])
        }
    }, [team, setTeam]);

    return <div>
        <h1>Team Builder With Memoisation</h1>
        <DependencyChangeCounter {...{addMember, deleteMember}} />

        <h2>Members</h2>
        <ul>
            {team.map(member => (
                <li>
                    {member}
                    <button onClick={() => deleteMember(member)}>Delete</button>
                </li>
            ))}
        </ul>

        <form>
                <div>
                    <label htmlFor='new-member-name'>New Member Name</label>
                    <input ref={txtNewMemberName} name='new-member-name'></input>
                </div>
                <button onClick={addMember}>Add</button>
        </form>
    </div>
}

export default TeamBuilder;