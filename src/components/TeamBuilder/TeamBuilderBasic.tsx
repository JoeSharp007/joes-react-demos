import { FC, MouseEventHandler, useRef, useState } from 'react';
import DependencyChangeCounter from './DependencyChangeCounter';

/**
 * This component demonstrates the most naive way to develop a react component.
 * It creates functions in the body with no control, so these functions will be regenerated
 * from scratch every time the component renders.
 * 
 * @returns Component
 */
const TeamBuilder: FC = () => {
    const [team, setTeam] = useState<string[]>(['alpha', 'bravo', 'charlie']);
    const txtNewMemberName = useRef<HTMLInputElement>(null);

    // Just to make it worse, I am using the current value of state to generate new values of state.
    const deleteMember = (member: string) => {
        setTeam(team.filter(m => m !== member));
    }

    const addMember: MouseEventHandler = (e) => {
        e.preventDefault();
        if (!!txtNewMemberName.current) {
            setTeam([...team, txtNewMemberName.current.value])
        }
    }

    return <div>
        <h1>Team Builder</h1>
        <DependencyChangeCounter {...{addMember, deleteMember}} />

        <h2>Members</h2>
        <ul>
            {team.map(member => (
                <li>
                    {member}
                    <button onClick={() => deleteMember(member)}>Delete</button>
                </li>
            ))}
        </ul>

        <form>
                <div>
                    <label htmlFor='new-member-name'>New Member Name</label>
                    <input ref={txtNewMemberName} name='new-member-name'></input>
                </div>
                <button onClick={addMember}>Add</button>
        </form>
    </div>
}

export default TeamBuilder;