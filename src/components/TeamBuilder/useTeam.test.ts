import { renderHook, act } from "@testing-library/react";
import useTeam from "./useTeam";

describe("useTeam", () => {
  it("should respect default values", () => {
    const { result } = renderHook(() => useTeam(["a", "b", "c"]));

    expect(result.current.team).toHaveLength(3);
    expect(result.current.team).toContain("a");
    expect(result.current.team).toContain("b");
    expect(result.current.team).toContain("c");
  });

  it("should add a team member correctly", () => {
    const { result } = renderHook(() => useTeam(["Rimmer", "Lister", "Cat"]));

    act(() => {
      result.current.addMember("Kryten");
    });

    expect(result.current.team).toHaveLength(4);
    expect(result.current.team).toContain("Kryten");
  });

  it("should delete a team member correctly", () => {
    const { result } = renderHook(() =>
      useTeam(["Rimmer", "Lister", "Cat", "Kochanski", "Kryten"])
    );

    act(() => {
      result.current.deleteMember("Kochanski");
    });

    expect(result.current.team).toHaveLength(4);
    expect(result.current.team).not.toContain("Kochanski");
  });
});
