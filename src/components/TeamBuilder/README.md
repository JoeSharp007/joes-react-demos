# React Knowledge Sharing

* Controlling the render with useMemo and useCallback
* Using custom hooks to modularise your code
* Using reducers to manage multiple inter-dependant items of state

I have created a component that allows the user to manage a list of team members.

I have implemented this in multiple ways, each better than the last, demonstrating some new principle.

The aim of these principles is:
* Allow for modularity
* Make code easier to test

The implementations are (in ascending order of goodness)

* Team Builder Basic
* Team Builder With Memo But Bad State
* Team Builder With Memo
* Team Builder With Reducer
* Team Builder With Custom Hook

There are comments in each file which outline the specific improvements being demonstrated.
