import { FC, MouseEventHandler, useRef, useReducer, useCallback } from 'react';
import DependencyChangeCounter from './DependencyChangeCounter';
import teamReducer from './teamReducer';

const INITIAL_TEAM = ['alpha', 'bravo', 'charlie']

/**
 * This component uses a reducer to contain the state.
 * 
 * Reducers are good if the state transitions are more complex, or the new values
 * tend to be mutations of the old values.
 * 
 * The dispatch function is something that will never change, the reducer itself is 
 * now separated out into something that can be independently tested.
 * 
 * I have also separated out the function to add a member, from the click handler.
 * So we have a general purpose 'addMember' function and then a distinct function
 * for handling the click, which knows how to extract the new member name from the UI.
 * 
 * @returns Component
 */
const TeamBuilder: FC = () => {
    const [team, dispatch] = useReducer(teamReducer, INITIAL_TEAM);

    const deleteMember = useCallback((member: string) => 
        dispatch({type: 'remove', member})
    , [])

    const addMember = useCallback((member: string) =>
        dispatch({type: 'add', member}), [])

    const txtNewMemberName = useRef<HTMLInputElement>(null);

    const onClickAdd: MouseEventHandler = useCallback((e) => {
        e.preventDefault();
        if (!!txtNewMemberName.current) {
            addMember(txtNewMemberName.current!.value);
        }
    }, [addMember, txtNewMemberName.current]);

    return <div>
        <h1>Team Builder With Reducer</h1>
        <DependencyChangeCounter {...{addMember, deleteMember}} />

        <h2>Members</h2>
        <ul>
            {team.map(member => (
                <li>
                    {member}
                    <button onClick={() => deleteMember(member)}>Delete</button>
                </li>
            ))}
        </ul>

        <form>
                <div>
                    <label htmlFor='new-member-name'>New Member Name</label>
                    <input ref={txtNewMemberName} name='new-member-name'></input>
                </div>
                <button onClick={onClickAdd}>Add</button>
        </form>
    </div>
}

export default TeamBuilder;