import { FC, useRef, useCallback, MouseEventHandler } from 'react';
import DependencyChangeCounter from './DependencyChangeCounter';

import useTeam from './useTeam';

/**
 * The reducer is nice, but perhaps we need to be able to manage teams of people 
 * in multiple places in our app? The component logic was getting rather long too
 * 
 * So, I have taken out all the functions that manipulated the reducer state into a separate 
 * custom hook. There are some rules with custom hooks 
 * https://www.w3schools.com/react/react_hooks.asp#:~:text=There%20are%203%20rules%20for%20hooks%3A%201%20Hooks,of%20a%20component.%203%20Hooks%20cannot%20be%20conditional
 * 
 * But this gives us yet another distinct module that can be tested without having to be inside a visual component.
 * 
 * @returns Component
 */
const TeamBuilder: FC = () => {
    const txtNewMemberName = useRef<HTMLInputElement>(null);

    const {team, addMember, deleteMember } = useTeam();

    const onClickAdd: MouseEventHandler = useCallback((e) => {
        e.preventDefault();
        if (!!txtNewMemberName.current) {
            addMember(txtNewMemberName.current!.value);
        }
    }, [addMember, txtNewMemberName.current]);

    return <div>
        <h1>Team Builder With Custom Hook</h1>
        <DependencyChangeCounter {...{addMember, deleteMember}} />

        <h2>Members</h2>
        <ul>
            {team.map(member => (
                <li>
                    {member}
                    <button onClick={() => deleteMember(member)}>Delete</button>
                </li>
            ))}
        </ul>

        <form>
                <div>
                    <label htmlFor='new-member-name'>New Member Name</label>
                    <input ref={txtNewMemberName} name='new-member-name'></input>
                </div>
                <button onClick={onClickAdd}>Add</button>
        </form>
    </div>
}

export default TeamBuilder;