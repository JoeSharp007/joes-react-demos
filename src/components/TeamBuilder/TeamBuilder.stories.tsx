import React, { FC } from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import TeamBuilderBasic from "./TeamBuilderBasic";
import TeamBuilderWithMemoButBadState from "./TeamBuilderWithMemoButBadState";
import TeamBuilderWithMemo from "./TeamBuilderWithMemo";
import TeamBuilderWithReducer from "./TeamBuilderWithReducer";
import TeamBuilderWithCustomHook from "./TeamBuilderWithCustomHook";

export default {
  title: "components/TeamBuilder",
  component: React.Component,
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof React.Component>;

export const Rough: ComponentStory<typeof TeamBuilderBasic> = () => <TeamBuilderBasic />;
export const WithMemoButBadState: ComponentStory<typeof TeamBuilderWithMemo> = () => <TeamBuilderWithMemoButBadState />;
export const WithMemo: ComponentStory<typeof TeamBuilderWithMemo> = () => <TeamBuilderWithMemo />;
export const WithReducer: ComponentStory<typeof TeamBuilderWithReducer> = () => <TeamBuilderWithReducer />;
export const WithCustomHook: ComponentStory<typeof TeamBuilderWithCustomHook> = () => <TeamBuilderWithCustomHook />;
