interface TeamAction {
    type: 'add' | 'remove',
    member: string;
}

const teamReducer = (team: string[], action: TeamAction): string[] => {
    switch (action.type) {
        case 'add':
            return [...team, action.member];
        case 'remove':
            return team.filter(n => n !== action.member);
    }
}

export default teamReducer;