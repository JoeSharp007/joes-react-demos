import { FC } from 'react';

import useMonitorDependency from '../../hooks/useMonitorDependency';

interface Props {
    addMember: unknown;
    deleteMember: unknown;
}

const DependencyChangeCounter: FC<Props> = ({addMember, deleteMember}) => {
    const countAddMember = useMonitorDependency(addMember);
    const countDeleteMember = useMonitorDependency(deleteMember);

    return (<div>
        <p>Add Member : {countAddMember}</p>
        <p>Delete Member : {countDeleteMember}</p>
        </div>) 
}

export default DependencyChangeCounter;