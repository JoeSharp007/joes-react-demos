import { useCallback, useReducer } from "react";
import teamReducer from "./teamReducer";

const INITIAL_TEAM = ['alpha', 'bravo', 'charlie']

interface UseTeam {
    team: string[];
    deleteMember: (name: string) => void;
    addMember: (name: string) => void
}

const useTeam = (initialTeam: string[] = INITIAL_TEAM): UseTeam => {
    const [team, dispatch] = useReducer(teamReducer, initialTeam);

    const deleteMember = useCallback((member: string) => 
        dispatch({type: 'remove', member})
    , [])

    const addMember = useCallback((member: string) =>
        dispatch({type: 'add', member}), [])

        return {
            team, deleteMember, addMember
        }
}

export default useTeam;