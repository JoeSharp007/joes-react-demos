import teamReducer from "./teamReducer";

describe("Team Reducer", () => {
    it ('adds a team member correctly', () => {
        const team = ['Charlie', 'Delta', 'Blue', 'Echo'];

        const result = teamReducer(team, {type: 'add', member: 'Indominus Rex'});

        expect(result).toHaveLength(5);
        expect(team).toHaveLength(4);
        expect(result).toContain('Indominus Rex')
    })

    it ('deletes a team member correctly', () => {
        const team = ['Charlie', 'Delta', 'Blue', 'Echo'];
        const result = teamReducer(team, {type: 'remove', member: 'Charlie'});

        expect(team).toHaveLength(4);
        expect(result).toHaveLength(3);
        expect(result).not.toContain('Charlie');

    })

    it ('does nothing if you delete a team member that doesn\'t exist', () => {
        const team = ['Charlie', 'Delta', 'Blue', 'Echo'];
        const result = teamReducer(team, {type: 'remove', member: 'Alan Grant'});

        expect(team).toHaveLength(4);
        expect(result).toHaveLength(4);
        expect(result).not.toContain('Alan Grant');

    })
});