import { FC } from "react";
import useCounter from "../../hooks/useCounter";

interface Props {
  initialValue?: number;
}

export const Counter: FC<Props> = ({ initialValue = 0 }) => {
  const { count, increment, decrement } = useCounter(initialValue);

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

export default Counter;
