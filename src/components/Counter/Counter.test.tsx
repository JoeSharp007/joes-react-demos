import React from "react";
import { act, render, screen, fireEvent } from "@testing-library/react";

import Counter from "./Counter";

describe("Counter", () => {
  it("should increment when button clicked", () => {
    render(<Counter />);

    const cmdIncrement = screen.getByText("Increment");

    act(() => {
      fireEvent.click(cmdIncrement);
      fireEvent.click(cmdIncrement);
      fireEvent.click(cmdIncrement);
    });

    expect(screen.getByText("Count: 3")).toBeVisible();
  });

  it("Should decrement when button clicked", () => {
    render(<Counter initialValue={10} />);

    const cmdDecrement = screen.getByText("Decrement");

    act(() => {
      fireEvent.click(cmdDecrement);
      fireEvent.click(cmdDecrement);
      fireEvent.click(cmdDecrement);
    });

    expect(screen.getByText("Count: 7")).toBeVisible();
  });
});
