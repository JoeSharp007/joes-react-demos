import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import Counter from "./Counter";

export default {
  title: "components/Counter",
  component: Counter,
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof Counter>;

const Template: ComponentStory<typeof Counter> = (args) => (
  <Counter {...args} />
);

export const Primary = Template.bind({});
export const LargeInitialValue = Template.bind({});
LargeInitialValue.args = {
  initialValue: 1000,
};
