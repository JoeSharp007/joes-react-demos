import TeamBuilder from "./components/TeamBuilder/TeamBuilderBasic";
import TeamBuilderWithMemo from "./components/TeamBuilder/TeamBuilderWithMemo";
import TeamBuilderWithReducer from "./components/TeamBuilder/TeamBuilderWithReducer";
import TeamBuilderWithCustomHook from "./components/TeamBuilder/TeamBuilderWithCustomHook";

function App() {
  return <>
    <TeamBuilder />
    <TeamBuilderWithMemo />
    <TeamBuilderWithReducer />
    <TeamBuilderWithCustomHook />
  </>;
}

export default App;
